import React from 'react'
import { View, Text, Image } from 'react-native'

const FooterComp = () => {
    return (
        <View>
            <Image 
            source={require('../assets/footer.jpg')}
            style={{width:430,height:40,}}
            />
        </View>
    )
}

export default FooterComp;