import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import ContentComp from './ContentComp'
import FooterComp from './FooterComp'
import HeaderComp from './HeaderComp'
import StorysComp from './StoryComp'

const App = () => {
  return (
    <View>
      <HeaderComp />
      <ScrollView style={{height:810,backgroundColor:'white'}}>
        <ScrollView horizontal>
          <StorysComp/>
          <StorysComp/>
          <StorysComp/>
          <StorysComp/>
          <StorysComp/>
          <StorysComp/>
          <StorysComp/>
        </ScrollView>
        <ContentComp />
        <ContentComp />
        <ContentComp />
        <ContentComp />
        <ContentComp />
        <ContentComp />
      </ScrollView>
      <FooterComp />
    </View>
  )
} 

export default App