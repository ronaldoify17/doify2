import React from 'react'
import { View, Text, Image } from 'react-native'

const HeaderComp = () => {
    return (
        <View style={{backgroundColor:'#fff',flexDirection:'row',height:50,}}>
            <View style={{flex:3,}}>
                <Image 
                source={require('../assets/ig.png')}
                style={{width:125,height:40,marginTop:5,marginLeft:10}}
                />
            </View>
            <View style={{flex:.5,}}>
                <Image 
                source={require('../assets/love.png')}
                style={{width:25,height:25,marginTop:10,paddingRight:5}}
                />
            </View>
            <View style={{flex:.5,}}>
                <Image 
                source={require('../assets/messenger.png')}
                style={{width:25,height:25,marginTop:10,}}
                />
            </View>
        </View>
    )
}

export default HeaderComp