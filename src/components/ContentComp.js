import React, { Component } from 'react'
import { Image, Text, View } from 'react-native'

export default class ContentComp extends Component {
    render() {
        return (
            <View>
               <Image 
                source={require('../assets/content.jpg')}
                style={{width:430,height:480,marginTop:5,}}
               />
            </View>
        )
    }
}