import React, { Component } from 'react'
import { Image, Text, View } from 'react-native'

export default class StorysComp extends Component {
    render() {
        return (
            <View style={{backgroundColor:'#fff'}}>
                <Image 
                    source={require('../assets/story.jpg')}
                    style={{width:70,height:70,borderRadius:35, borderWidth: 3,borderColor:'red',marginTop:10,marginLeft:15,marginBottom:5,}}
                />
            </View>
        )
    }
}